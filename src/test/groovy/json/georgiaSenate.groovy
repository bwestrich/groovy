package json

/*
  Purpose: uses partial election results to forecast the final results of Georgia senate elections. a
  Projection assumes that proportion of votes received by each candidate so far will remain constant as remaining uncounted
  votes are counted.
 */

import groovy.json.JsonSlurper

def getResults(String uri) {
    URL url = new URL('https', 'politics-elex-results.data.api.cnn.io',
            uri)
    JsonSlurper slurper = new JsonSlurper()
    return slurper.parse(url)
}

def projectSenateResults(def race) {

    def result = getResults("results/view/2020-county-races-${race}-GA.json")

//// optionally sort senate results to find counties with low percent reporting
//    result.sort{it.percentReporting}

//// export results to excel for testing
//result.each {
//    def elephant = it.candidates.find { it.firstName == 'Donald'}
//    def donkey = it.candidates.find { it.firstName == 'Joe'}
//    println "$it.countyName, $it.percentReporting, ${elephant.voteNum}, ${donkey.voteNum}"
//}

    def raceName = (race == "SW")? "Perdue vs. Ossoff": "Loeffler vs. Warnock"
    float elephantVotes = 0, elephantProjectedVotes = 0, donkeyVotes = 0, donkeyProjectedVotes = 0
    boolean allCountiesReported = true
    try {
        result.each {
            if (it.percentReporting == 0) {
                //throw new IllegalArgumentException("Georgia county '$it.countyName' for race '$raceName' has no votes reported, cannot project state results.")
                allCountiesReported = false
                return
            }
            def elephant = it.candidates.find { it.majorParty == 'REP' }
            def donkey = it.candidates.find { it.majorParty == 'DEM' }
            elephantVotes += elephant.voteNum
            donkeyVotes += donkey.voteNum
            elephantProjectedVotes += elephant.voteNum / it.percentReporting * 100
            donkeyProjectedVotes += donkey.voteNum / it.percentReporting * 100
        }
    } catch (IllegalArgumentException e) {
        println e.getMessage()
        return
    }
    float totalVotes = elephantVotes + donkeyVotes
    float projectedTotalVotes = elephantProjectedVotes + donkeyProjectedVotes
    float projectedUncountedVotes = projectedTotalVotes - totalVotes
    float elephantProjectedPercent = elephantProjectedVotes / projectedTotalVotes * 100
    float donkeyProjectedPercent = donkeyProjectedVotes / projectedTotalVotes * 100
    float victoryMargin = Math.abs(elephantProjectedVotes - donkeyProjectedVotes)
    float victoryPercent = Math.abs(elephantProjectedPercent - donkeyProjectedPercent)
    float uncountedPercent  = projectedUncountedVotes / projectedTotalVotes * 100
    def projectedWinner = elephantProjectedVotes > donkeyProjectedVotes? "elephant" : "donkey"
    def incompleteWarning = allCountiesReported? "" : "     WARNING: not all counties have reported results. "
    println "// $raceName:  elephant=$elephantProjectedVotes ($elephantProjectedPercent%) donkey=$donkeyProjectedVotes ($donkeyProjectedPercent%) Total=$projectedTotalVotes" +
            " difference=$victoryMargin ($victoryPercent%)" +
            " [$projectedWinner wins $race]" +
            " uncounted=$projectedUncountedVotes ($uncountedPercent%) " +

            " elephantVotes=$elephantVotes donkeyVotes=$donkeyVotes" +
            " currentDiff=${Math.abs(elephantVotes - donkeyVotes)} " +
            incompleteWarning
}

println "// Projections for: " + new Date()

projectSenateResults('SW')
projectSenateResults('S2W')

// Projections for: Tue Jan 05 18:45:04 CST 2021
// Perdue vs. Ossoff:  elephant=641629.75 (39.06518%) donkey=1000829.6 (60.93482%) Total=1642459.4 difference=359199.88 (21.869637%) [donkey wins SW] uncounted=1073224.4 (65.34252%)  currentDiff=22659.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=641045.2 (38.797382%) donkey=1011244.56 (61.202618%) Total=1652289.8 difference=370199.38 (22.405235%) [donkey wins S2W] uncounted=1083500.8 (65.575714%)  currentDiff=26009.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 18:47:18 CST 2021
// Perdue vs. Ossoff:  elephant=726829.1 (41.498997%) donkey=1024608.56 (58.501%) Total=1751437.8 difference=297779.44 (17.002003%) [donkey wins SW] uncounted=1135961.8 (64.85881%)  currentDiff=2394.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=674491.0 (39.827915%) donkey=1019022.25 (60.172085%) Total=1693513.2 difference=344531.25 (20.34417%) [donkey wins S2W] uncounted=1113696.2 (65.762474%)  currentDiff=19695.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 18:51:18 CST 2021
// Perdue vs. Ossoff:  elephant=789786.1 (36.887554%) donkey=1351277.9 (63.112446%) Total=2141064.0 difference=561491.75 (26.224892%) [donkey wins SW] uncounted=1337740.0 (62.48015%)  currentDiff=68398.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=788421.8 (36.64398%) donkey=1363150.9 (63.35602%) Total=2151572.8 difference=574729.06 (26.712044%) [donkey wins S2W] uncounted=1348302.8 (62.665916%)  currentDiff=73010.0      WARNING: not all counties have reported results.
// CNN reported Osshoff 395,064, Perdue 367,463

// Projections for: Tue Jan 05 18:54:21 CST 2021
// Perdue vs. Ossoff:  elephant=807724.0 (37.21318%) donkey=1362808.0 (62.78682%) Total=2170532.0 difference=555084.0 (25.573639%) [donkey wins SW] uncounted=1346976.0 (62.05741%)  currentDiff=62222.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=806452.44 (36.97598%) donkey=1374564.8 (63.02402%) Total=2181017.2 difference=568112.3 (26.048042%) [donkey wins S2W] uncounted=1357511.2 (62.242115%)  currentDiff=66778.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 18:56:11 CST 2021
// Perdue vs. Ossoff:  elephant=807933.5 (37.222706%) donkey=1362606.1 (62.7773%) Total=2170539.5 difference=554672.6 (25.554596%) [donkey wins SW] uncounted=1346409.5 (62.031097%)  elephantVotes=380983.0 donkeyVotes=443147.0 currentDiff=62164.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=806658.3 (36.985313%) donkey=1374365.0 (63.014687%) Total=2181023.2 difference=567706.7 (26.029373%) [donkey wins S2W] uncounted=1356944.2 (62.215946%)  elephantVotes=378679.0 donkeyVotes=445400.0 currentDiff=66721.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 19:13:19 CST 2021
// Perdue vs. Ossoff:  elephant=1174393.8 (39.49058%) donkey=1799464.0 (60.50942%) Total=2973857.8 difference=625070.25 (21.018837%) [donkey wins SW] uncounted=1677243.8 (56.399597%)  elephantVotes=618522.0 donkeyVotes=678092.0 currentDiff=59570.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=1168260.4 (39.15568%) donkey=1815368.9 (60.84432%) Total=2983629.2 difference=647108.5 (21.688637%) [donkey wins S2W] uncounted=1687360.2 (56.55395%)  elephantVotes=614181.0 donkeyVotes=682088.0 currentDiff=67907.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 19:30:15 CST 2021
// Perdue vs. Ossoff:  elephant=1507492.5 (43.25475%) donkey=1977656.5 (56.74525%) Total=3485149.0 difference=470164.0 (13.490501%) [donkey wins SW] uncounted=1608738.0 (46.159805%)  elephantVotes=861188.0 donkeyVotes=1015223.0 currentDiff=154035.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=1486745.1 (42.554615%) donkey=2006989.1 (57.445385%) Total=3493734.2 difference=520244.0 (14.89077%) [donkey wins S2W] uncounted=1616221.2 (46.260567%)  elephantVotes=854980.0 donkeyVotes=1022533.0 currentDiff=167553.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 19:36:49 CST 2021
// Perdue vs. Ossoff:  elephant=1664671.9 (44.9358%) donkey=2039884.1 (55.0642%) Total=3704556.0 difference=375212.25 (10.128403%) [donkey wins SW] uncounted=1751231.0 (47.272358%)  elephantVotes=898042.0 donkeyVotes=1055283.0 currentDiff=157241.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=1636507.9 (44.17444%) donkey=2068141.2 (55.825565%) Total=3704649.0 difference=431633.38 (11.651127%) [donkey wins S2W] uncounted=1751174.0 (47.269634%)  elephantVotes=890706.0 donkeyVotes=1062769.0 currentDiff=172063.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 19:41:08 CST 2021
// Perdue vs. Ossoff:  elephant=1823890.4 (46.647522%) donkey=2086050.2 (53.35248%) Total=3909940.5 difference=262159.88 (6.70496%) [donkey wins SW] uncounted=1815223.5 (46.42586%)  elephantVotes=977581.0 donkeyVotes=1117136.0 currentDiff=139555.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=1795662.5 (45.922436%) donkey=2114545.0 (54.077564%) Total=3910207.5 difference=318882.5 (8.1551285%) [donkey wins S2W] uncounted=1815308.5 (46.424862%)  elephantVotes=969927.0 donkeyVotes=1124972.0 currentDiff=155045.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 19:52:29 CST 2021
// Perdue vs. Ossoff:  elephant=1928604.4 (47.63683%) donkey=2119953.2 (52.363174%) Total=4048557.5 difference=191348.88 (4.726345%) [donkey wins SW] uncounted=1748504.5 (43.18833%)  elephantVotes=1102369.0 donkeyVotes=1197684.0 currentDiff=95315.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=1869149.4 (46.57572%) donkey=2143991.5 (53.424274%) Total=4013141.0 difference=274842.12 (6.8485527%) [donkey wins S2W] uncounted=1713695.0 (42.702087%)  elephantVotes=1093340.0 donkeyVotes=1206106.0 currentDiff=112766.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 20:02:02 CST 2021.  Current margins tighten, but projected margins widen
// Perdue vs. Ossoff:  elephant=1950578.2 (47.444836%) donkey=2160677.0 (52.555164%) Total=4111255.2 difference=210098.75 (5.1103287%) [donkey wins SW] uncounted=1577396.2 (38.367752%)  elephantVotes=1261387.0 donkeyVotes=1272472.0 currentDiff=11085.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=1870891.1 (46.147743%) donkey=2183242.5 (53.85226%) Total=4054133.5 difference=312351.38 (7.7045174%) [donkey wins S2W] uncounted=1521066.5 (37.518906%)  elephantVotes=1251399.0 donkeyVotes=1281668.0 currentDiff=30269.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 20:06:23 CST 2021.  Same trend continues
// Perdue vs. Ossoff:  elephant=1932962.8 (46.9552%) donkey=2183648.0 (53.0448%) Total=4116610.8 difference=250685.25 (6.0895996%) [donkey wins SW] uncounted=1492439.8 (36.25409%)  elephantVotes=1309438.0 donkeyVotes=1314733.0 currentDiff=5295.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=1866215.4 (45.97118%) donkey=2193318.0 (54.02882%) Total=4059533.5 difference=327102.62 (8.05764%) [donkey wins S2W] uncounted=1436004.5 (35.373634%)  elephantVotes=1299454.0 donkeyVotes=1324075.0 currentDiff=24621.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 20:14:55 CST 2021.  Same trend continues
// Perdue vs. Ossoff:  elephant=1988983.9 (47.487484%) donkey=2199454.2 (52.512516%) Total=4188438.0 difference=210470.38 (5.025032%) [donkey wins SW] uncounted=1419709.0 (33.895905%)  elephantVotes=1388704.0 donkeyVotes=1380025.0 currentDiff=8679.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=1920159.1 (46.49662%) donkey=2209515.5 (53.50338%) Total=4129674.5 difference=289356.38 (7.0067596%) [donkey wins S2W] uncounted=1371174.5 (33.20297%)  elephantVotes=1370281.0 donkeyVotes=1388219.0 currentDiff=17938.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 20:20:33 CST 2021.  Projected margins tighten.
// Perdue vs. Ossoff:  elephant=2069875.4 (48.031185%) donkey=2239565.2 (51.96882%) Total=4309440.5 difference=169689.88 (3.9376335%) [donkey wins SW] uncounted=1448042.5 (33.601635%)  elephantVotes=1437828.0 donkeyVotes=1423570.0 currentDiff=14258.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=2000851.2 (47.055393%) donkey=2251267.8 (52.944607%) Total=4252119.0 difference=250416.5 (5.8892136%) [donkey wins S2W] uncounted=1396692.0 (32.846966%)  elephantVotes=1421558.0 donkeyVotes=1433869.0 currentDiff=12311.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 20:26:24 CST 2021.  Current margins tighten, but projected margins widen
// Perdue vs. Ossoff:  elephant=2055913.1 (47.77007%) donkey=2247855.5 (52.229935%) Total=4303768.5 difference=191942.38 (4.4598656%) [donkey wins SW] uncounted=1401400.5 (32.562172%)  elephantVotes=1463473.0 donkeyVotes=1438895.0 currentDiff=24578.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=2004487.4 (46.99234%) donkey=2261074.5 (53.007656%) Total=4265562.0 difference=256587.12 (6.015316%) [donkey wins S2W] uncounted=1363846.0 (31.97342%)  elephantVotes=1452094.0 donkeyVotes=1449622.0 currentDiff=2472.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 20:32:23 CST 2021.  Current margins tighten, but projected margins widen
// Perdue vs. Ossoff:  elephant=2081918.1 (48.289665%) donkey=2229394.0 (51.71034%) Total=4311312.0 difference=147475.88 (3.4206734%) [donkey wins SW] uncounted=1274014.0 (29.550493%)  elephantVotes=1539031.0 donkeyVotes=1498267.0 currentDiff=40764.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=2031770.8 (47.548805%) donkey=2241250.8 (52.451195%) Total=4273021.5 difference=209480.0 (4.9023895%) [donkey wins S2W] uncounted=1236230.5 (28.931063%)  elephantVotes=1527015.0 donkeyVotes=1509776.0 currentDiff=17239.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 20:37:21 CST 2021   Projected margins tighten.
// Perdue vs. Ossoff:  elephant=2104251.0 (48.797577%) donkey=2207952.8 (51.20242%) Total=4312204.0 difference=103701.75 (2.4048424%) [donkey wins SW] uncounted=1028480.0 (23.850449%)  elephantVotes=1660627.0 donkeyVotes=1623097.0 currentDiff=37530.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=2055157.0 (48.08244%) donkey=2219079.2 (51.917564%) Total=4274236.0 difference=163922.25 (3.835125%) [donkey wins S2W] uncounted=991017.0 (23.185827%)  elephantVotes=1647623.0 donkeyVotes=1635596.0 currentDiff=12027.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 20:42:53 CST 2021
// Perdue vs. Ossoff:  elephant=2117299.0 (48.974804%) donkey=2205942.5 (51.025196%) Total=4323241.5 difference=88643.5 (2.0503922%) [donkey wins SW] uncounted=985981.5 (22.806534%)  elephantVotes=1697611.0 donkeyVotes=1639649.0 currentDiff=57962.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=2068477.4 (48.264347%) donkey=2217248.0 (51.735653%) Total=4285725.5 difference=148770.62 (3.4713058%) [donkey wins S2W] uncounted=948974.5 (22.142681%)  elephantVotes=1684451.0 donkeyVotes=1652300.0 currentDiff=32151.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 20:51:41 CST 2021
// Perdue vs. Ossoff:  elephant=2147119.0 (49.362835%) donkey=2202547.8 (50.637157%) Total=4349667.0 difference=55428.75 (1.2743225%) [donkey wins SW] uncounted=913347.0 (20.99809%)  elephantVotes=1746216.0 donkeyVotes=1690104.0 currentDiff=56112.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=2097572.5 (48.65222%) donkey=2213787.2 (51.34777%) Total=4311360.0 difference=116214.75 (2.695549%) [donkey wins S2W] uncounted=875562.0 (20.308256%)  elephantVotes=1732517.0 donkeyVotes=1703281.0 currentDiff=29236.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 21:03:02 CST 2021
// Perdue vs. Ossoff:  elephant=2173318.2 (49.61719%) donkey=2206853.8 (50.38281%) Total=4380172.0 difference=33535.5 (0.7656174%) [donkey wins SW] uncounted=868473.0 (19.827372%)  elephantVotes=1792150.0 donkeyVotes=1719549.0 currentDiff=72601.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=2124325.8 (48.912952%) donkey=2218748.0 (51.08704%) Total=4343074.0 difference=94422.25 (2.1740875%) [donkey wins S2W] uncounted=831910.0 (19.154865%)  elephantVotes=1778316.0 donkeyVotes=1732848.0 currentDiff=45468.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 21:03:02 CST 2021
// Perdue vs. Ossoff:  elephant=2173318.2 (49.61719%) donkey=2206853.8 (50.38281%) Total=4380172.0 difference=33535.5 (0.7656174%) [donkey wins SW] uncounted=868473.0 (19.827372%)  elephantVotes=1792150.0 donkeyVotes=1719549.0 currentDiff=72601.0      WARNING: not all counties have reported results.
// Loeffler vs. Warnock:  elephant=2124325.8 (48.912952%) donkey=2218748.0 (51.08704%) Total=4343074.0 difference=94422.25 (2.1740875%) [donkey wins S2W] uncounted=831910.0 (19.154865%)  elephantVotes=1778316.0 donkeyVotes=1732848.0 currentDiff=45468.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 21:07:57 CST 2021
// Perdue vs. Ossoff:  elephant=2199099.2 (49.848366%) donkey=2212478.2 (50.151634%) Total=4411577.5 difference=13379.0 (0.30326843%) [donkey wins SW] uncounted=798558.5 (18.101427%)  elephantVotes=1847596.0 donkeyVotes=1765423.0 currentDiff=82173.0
// Loeffler vs. Warnock:  elephant=2148921.8 (49.1367%) donkey=2224432.2 (50.8633%) Total=4373354.0 difference=75510.5 (1.7266006%) [donkey wins S2W] uncounted=760893.0 (17.398386%)  elephantVotes=1832928.0 donkeyVotes=1779533.0 currentDiff=53395.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 21:19:07 CST 2021. Perdue projected to win!
// Perdue vs. Ossoff:  elephant=2221614.5 (50.282887%) donkey=2196617.5 (49.717113%) Total=4418232.0 difference=24997.0 (0.565773%) [elephant wins SW] uncounted=698688.0 (15.813746%)  elephantVotes=1908865.0 donkeyVotes=1810679.0 currentDiff=98186.0
// Loeffler vs. Warnock:  elephant=2171393.5 (49.57583%) donkey=2208550.2 (50.424168%) Total=4379944.0 difference=37156.75 (0.8483391%) [donkey wins S2W] uncounted=660976.0 (15.090969%)  elephantVotes=1893512.0 donkeyVotes=1825456.0 currentDiff=68056.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 21:25:17 CST 2021
// Perdue vs. Ossoff:  elephant=2229356.5 (50.414295%) donkey=2192715.5 (49.585705%) Total=4422072.0 difference=36641.0 (0.8285904%) [elephant wins SW] uncounted=673526.0 (15.231005%)  elephantVotes=1923236.0 donkeyVotes=1825310.0 currentDiff=97926.0
// Loeffler vs. Warnock:  elephant=2178642.0 (49.701218%) donkey=2204836.0 (50.298782%) Total=4383478.0 difference=26194.0 (0.5975647%) [donkey wins S2W] uncounted=635516.0 (14.497986%)  elephantVotes=1907598.0 donkeyVotes=1840364.0 currentDiff=67234.0      WARNING: not all counties have reported results.

// Projections for: Tue Jan 05 21:38:43 CST 2021.  Loeffler projected to win!
// Perdue vs. Ossoff:  elephant=2225784.8 (50.480526%) donkey=2183410.0 (49.51947%) Total=4409195.0 difference=42374.75 (0.96105576%) [elephant wins SW] uncounted=586228.0 (13.295579%)  elephantVotes=1966228.0 donkeyVotes=1856739.0 currentDiff=109489.0
// Loeffler vs. Warnock:  elephant=2207329.2 (50.065033%) donkey=2201594.5 (49.934963%) Total=4408924.0 difference=5734.75 (0.13006973%) [elephant wins S2W] uncounted=585752.0 (13.2856%)  elephantVotes=1950991.0 donkeyVotes=1872181.0 currentDiff=78810.0

// Projections for: Tue Jan 05 22:23:06 CST 2021
// Perdue vs. Ossoff:  elephant=2247586.0 (50.091454%) donkey=2239379.0 (49.908546%) Total=4486965.0 difference=8207.0 (0.1829071%) [elephant wins SW] uncounted=260516.0 (5.8060627%)  elephantVotes=2123039.0 donkeyVotes=2103410.0 currentDiff=19629.0
// Loeffler vs. Warnock:  elephant=2224583.5 (49.59701%) donkey=2260734.0 (50.40299%) Total=4485317.5 difference=36150.5 (0.80597687%) [donkey wins S2W] uncounted=263677.5 (5.8786807%)  elephantVotes=2100815.0 donkeyVotes=2120825.0 currentDiff=20010.0

// Projections for: Wed Jan 06 06:32:46 CST 2021.  Projections show both Dems will win by more than a 0.5%, though current margins are thin for Ossoff
// Perdue vs. Ossoff:  elephant=2282568.5 (49.657146%) donkey=2314088.0 (50.342854%) Total=4596656.5 difference=31519.5 (0.6857071%) [donkey wins SW] uncounted=195592.5 (4.2551036%)  elephantVotes=2192347.0 donkeyVotes=2208717.0 currentDiff=16370.0
// Loeffler vs. Warnock:  elephant=2263218.0 (49.23552%) donkey=2333499.8 (50.764473%) Total=4596718.0 difference=70281.75 (1.5289536%) [donkey wins S2W] uncounted=195556.0 (4.254253%)  elephantVotes=2173866.0 donkeyVotes=2227296.0 currentDiff=53430.0

// Projections for: Wed Jan 06 08:59:21 CST 2021.  Same trend as before.
// Perdue vs. Ossoff:  elephant=2263784.0 (49.51524%) donkey=2308109.5 (50.48476%) Total=4571893.5 difference=44325.5 (0.96952057%) [donkey wins SW] uncounted=165712.5 (3.6245923%)  elephantVotes=2194578.0 donkeyVotes=2211603.0 currentDiff=17025.0
// Loeffler vs. Warnock:  elephant=2244441.8 (49.091812%) donkey=2327485.0 (50.908184%) Total=4571927.0 difference=83043.25 (1.8163719%) [donkey wins S2W] uncounted=165648.0 (3.6231549%)  elephantVotes=2176048.0 donkeyVotes=2230231.0 currentDiff=54183.0

// Projections for: Wed Jan 06 15:45:37 CST 2021.  Race now called for ossoff
// Perdue vs. Ossoff:  elephant=2261798.0 (49.419876%) donkey=2314899.2 (50.580128%) Total=4576697.0 difference=53101.25 (1.1602516%) [donkey wins SW] uncounted=152342.0 (3.328645%)  elephantVotes=2198640.0 donkeyVotes=2225715.0 currentDiff=27075.0
// Loeffler vs. Warnock:  elephant=2242334.8 (48.99386%) donkey=2334432.5 (51.006145%) Total=4576767.0 difference=92097.75 (2.0122871%) [donkey wins S2W] uncounted=152341.0 (3.3285723%)  elephantVotes=2179969.0 donkeyVotes=2244457.0 currentDiff=64488.0
