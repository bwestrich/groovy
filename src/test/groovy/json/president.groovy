package json

/*
  Purpose: uses president partial election results to forecast the final results of Presidential election. a
  Projection assumes that proportion of votes received by each candidate so far will remain constant as remaining uncounted
  votes are counted.
 */

import groovy.json.JsonSlurper

def projectStateResults(def stateCode) {

    // got this url by inspecting CNN pages that display president results
    URL url = new URL('https', 'politics-elex-results.data.api.cnn.io',
            "results/view/2020-president-races-PG-${stateCode}.json")

    JsonSlurper slurper = new JsonSlurper()
    def result = slurper.parse(url)

//// optionally sort president results to find counties with low percent reporting
//    result.sort{it.percentReporting}

//// export results to excel for testing
//result.each {
//    def trump = it.candidates.find { it.firstName == 'Donald'}
//    def biden = it.candidates.find { it.firstName == 'Joe'}
//    println "$it.countyName, $it.percentReporting, ${trump.voteNum}, ${biden.voteNum}"
//}

    float trumpVotes = 0, trumpProjectedVotes = 0, bidenVotes = 0, bidenProjectedVotes = 0
    try {
        result.each {
            if (it.percentReporting == 0) {
                throw new IllegalArgumentException("County $it.countyName in state $stateCode has no votes reported, cannot project state results")
            }
            def trump = it.candidates.find { it.firstName == 'Donald' }
            def biden = it.candidates.find { it.firstName == 'Joe' }
            trumpVotes += trump.voteNum
            bidenVotes += biden.voteNum
            trumpProjectedVotes += trump.voteNum / it.percentReporting * 100
            bidenProjectedVotes += biden.voteNum / it.percentReporting * 100
        }
    } catch (IllegalArgumentException e) {
        println e.getMessage()
        return
    }
    float totalVotes = trumpVotes + bidenVotes
    float projectedTotalVotes = trumpProjectedVotes + bidenProjectedVotes
    float projectedUncountedVotes = projectedTotalVotes - totalVotes
    float trumpProjectedPercent = trumpProjectedVotes / projectedTotalVotes * 100
    float bidenProjectedPercent = bidenProjectedVotes / projectedTotalVotes * 100
    float victoryMargin = Math.abs(trumpProjectedVotes - bidenProjectedVotes)
    float victoryPercent = Math.abs(trumpProjectedPercent - bidenProjectedPercent)
    float uncountedPercent  = projectedUncountedVotes / projectedTotalVotes * 100
    def projectedWinner = trumpProjectedVotes > bidenProjectedVotes? "Trump" : "Biden"
    println "// $stateCode:  Trump=$trumpProjectedVotes ($trumpProjectedPercent%) Biden=$bidenProjectedVotes ($bidenProjectedPercent%) Total=$projectedTotalVotes" +
            " difference=$victoryMargin ($victoryPercent%)" +
            " [$projectedWinner wins $stateCode]" +
            " uncounted=$projectedUncountedVotes ($uncountedPercent%) " +

            //" trumpVotes=$trumpVotes bidenVotes=$bidenVotes"
            " currentDiff=${Math.abs(trumpVotes - bidenVotes)} "
}

println "// Projections for: " + new Date()
// states that have already been called
//projectStateResults('MI')

// states not yet called by AP as of end of day Nov 4
projectStateResults('AZ')
projectStateResults('GA')
projectStateResults('NC')
projectStateResults('NV')
projectStateResults('PA')



// Projections for: Wed Nov 04 23:32:24 CST 2020
// AZ:  Trump=1609578.6 (48.585434%)  Biden=1703304.2 (51.414562%) Total=3312883.0 difference=93725.625 (2.8291283%) [Biden wins AZ]
// GA:  Trump=2571101.0 (49.94337%)  Biden=2576931.5 (50.05663%) Total=5148032.5 difference=5830.5 (0.11325836%) [Biden wins GA]
// NC:  Trump=2872674.5 (50.614185%)  Biden=2802956.5 (49.385815%) Total=5675631.0 difference=69718.0 (1.2283707%) [Trump wins NC]
// NV:  Trump=677709.8 (49.553566%)  Biden=689920.9 (50.44643%) Total=1367630.8 difference=12211.0625 (0.8928642%) [Biden wins NV]
// PA:  Trump=3569655.2 (50.412014%)  Biden=3511306.2 (49.587986%) Total=7080961.5 difference=58349.0 (0.824028%) [Trump wins PA]

// Projections for: Thu Nov 05 10:43:28 CST 2020
// AZ:  Trump=1617622.0 (48.828766%)  Biden=1695224.2 (51.171234%) Total=3312846.2 difference=77602.25 (2.3424683%) [Biden wins AZ]
// GA:  Trump=2553636.2 (50.04695%)  Biden=2548844.5 (49.95304%) Total=5102481.0 difference=4791.75 (0.09391022%) [Trump wins GA]
// NC:  Trump=2872674.5 (50.614185%)  Biden=2802956.5 (49.385815%) Total=5675631.0 difference=69718.0 (1.2283707%) [Trump wins NC]
// NV:  Trump=677709.8 (49.553566%)  Biden=689920.9 (50.44643%) Total=1367630.8 difference=12211.0625 (0.8928642%) [Biden wins NV]
// PA:  Trump=3530410.2 (50.68009%)  Biden=3435658.5 (49.319904%) Total=6966069.0 difference=94751.75 (1.3601875%) [Trump wins PA]

// Projections for: Thu Nov 05 12:12:43 CST 2020
// AZ:  Trump=1617622.0 (48.828766%)  Biden=1695224.2 (51.171234%) Total=3312846.2 difference=77602.25 (2.3424683%) [Biden wins AZ]
// GA:  Trump=2555184.5 (50.041126%)  Biden=2550984.2 (49.95887%) Total=5106169.0 difference=4200.25 (0.08225632%) [Trump wins GA]
// NC:  Trump=2870172.0 (50.63125%)  Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC]
// NV:  Trump=678224.0 (49.40584%)  Biden=694536.75 (50.59416%) Total=1372760.8 difference=16312.75 (1.1883163%) [Biden wins NV]
// PA:  Trump=3524113.0 (50.599377%)  Biden=3440623.2 (49.400627%) Total=6964736.0 difference=83489.75 (1.1987495%) [Trump wins PA]

// Projections for: Thu Nov 05 12:20:35 CST 2020
// AZ:  Trump=1617622.0 (48.828766%)  Biden=1695224.2 (51.171234%) Total=3312846.2 difference=77602.25 (2.3424683%) [Biden wins AZ]
// GA:  Trump=2555184.5 (50.041126%)  Biden=2550984.2 (49.95887%) Total=5106169.0 difference=4200.25 (0.08225632%) [Trump wins GA]
// NC:  Trump=2870172.0 (50.63125%)  Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC]
// NV:  Trump=663493.56 (49.488243%)  Biden=677216.0 (50.51176%) Total=1340709.5 difference=13722.4375 (1.0235176%) [Biden wins NV]
// PA:  Trump=3522240.0 (50.58221%)  Biden=3441157.0 (49.41779%) Total=6963397.0 difference=81083.0 (1.1644211%) [Trump wins PA]

// Projections for: Thu Nov 05 14:26:31 CST 2020
// AZ:  Trump=1617758.4 (48.82608%)  Biden=1695549.6 (51.17392%) Total=3313308.0 difference=77791.25 (2.3478394%) [Biden wins AZ]
// GA:  Trump=2502023.5 (49.873985%)  Biden=2514667.0 (50.126015%) Total=5016690.5 difference=12643.5 (0.25202942%) [Biden wins GA]
// NC:  Trump=2870172.0 (50.63125%)  Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC]
// NV:  Trump=663493.56 (49.488243%)  Biden=677216.0 (50.51176%) Total=1340709.5 difference=13722.4375 (1.0235176%) [Biden wins NV]
// PA:  Trump=3519222.5 (50.515778%)  Biden=3447358.5 (49.484222%) Total=6966581.0 difference=71864.0 (1.0315552%) [Trump wins PA]

// Projections for: Thu Nov 05 16:02:12 CST 2020
// AZ:  Trump=1617873.6 (48.828743%)  Biden=1695489.6 (51.171257%) Total=3313363.2 difference=77616.0 (2.342514%) [Biden wins AZ]
// GA:  Trump=2502312.5 (49.82347%)  Biden=2520044.5 (50.17653%) Total=5022357.0 difference=17732.0 (0.35305786%) [Biden wins GA]
// NC:  Trump=2870172.0 (50.63125%)  Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC]
// NV:  Trump=663493.56 (49.488243%)  Biden=677216.0 (50.51176%) Total=1340709.5 difference=13722.4375 (1.0235176%) [Biden wins NV]
// PA:  Trump=3508392.0 (50.41985%)  Biden=3449962.8 (49.580147%) Total=6958355.0 difference=58429.25 (0.8397026%) [Trump wins PA]

// Projections for: Thu Nov 05 16:58:51 CST 2020
// AZ:  Trump=1610241.1 (48.80827%)  Biden=1688874.1 (51.19173%) Total=3299115.2 difference=78633.0 (2.383461%) [Biden wins AZ]
// GA:  Trump=2502117.5 (49.82153%)  Biden=2520043.8 (50.178474%) Total=5022161.0 difference=17926.25 (0.35694504%) [Biden wins GA]
// NC:  Trump=2870172.0 (50.63125%)  Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC]
// NV:  Trump=663493.56 (49.488243%)  Biden=677216.0 (50.51176%) Total=1340709.5 difference=13722.4375 (1.0235176%) [Biden wins NV]
// PA:  Trump=3499739.8 (50.26656%)  Biden=3462622.0 (49.733437%) Total=6962362.0 difference=37117.75 (0.533123%) [Trump wins PA]

// Projections for: Thu Nov 05 18:51:57 CST 2020
// AZ:  Trump=1615223.2 (48.895412%)  Biden=1688201.9 (51.10459%) Total=3303425.0 difference=72978.625 (2.209179%) [Biden wins AZ]
// GA:  Trump=2504757.2 (49.77648%)  Biden=2527252.2 (50.22352%) Total=5032009.5 difference=22495.0 (0.44703674%) [Biden wins GA]
// NC:  Trump=2870172.0 (50.63125%)  Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC]
// NV:  Trump=663493.56 (49.488243%)  Biden=677216.0 (50.51176%) Total=1340709.5 difference=13722.4375 (1.0235176%) [Biden wins NV]
// PA:  Trump=3496458.5 (50.26042%)  Biden=3460225.0 (49.73958%) Total=6956683.5 difference=36233.5 (0.5208435%) [Trump wins PA]

// Projections for: Thu Nov 05 23:25:37 CST 2020
// AZ:  Trump=1640907.5 (49.220726%) Biden=1692866.1 (50.779278%) Total=3333773.5 difference=51958.625 (1.5585518%) [Biden wins AZ] uncounted=322222.5 (9.665399%)
// GA:  Trump=2501547.2 (49.851887%) Biden=2516411.8 (50.148113%) Total=5017959.0 difference=14864.5 (0.2962265%) [Biden wins GA] uncounted=123732.0 (2.4657834%)
// NC:  Trump=2870172.0 (50.63125%) Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC] uncounted=281273.0 (4.9617944%)
// NV:  Trump=663493.56 (49.488243%) Biden=677216.0 (50.51176%) Total=1340709.5 difference=13722.4375 (1.0235176%) [Biden wins NV] uncounted=143645.5 (10.714141%)
// PA:  Trump=3478286.5 (50.036537%) Biden=3473206.5 (49.963463%) Total=6951493.0 difference=5080.0 (0.07307434%) [Trump wins PA] uncounted=403179.0 (5.799891%)

// Projections for: Fri Nov 06 08:12:58 CST 2020
// AZ:  Trump=1641630.9 (49.236362%) Biden=1692553.1 (50.763638%) Total=3334184.0 difference=50922.25 (1.5272751%) [Biden wins AZ] uncounted=317112.0 (9.510933%)
// GA:  Trump=2502040.8 (49.82159%) Biden=2519960.0 (50.178406%) Total=5022001.0 difference=17919.25 (0.35681534%) [Biden wins GA] uncounted=123934.0 (2.4678211%)
// NC:  Trump=2870172.0 (50.63125%) Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC] uncounted=281273.0 (4.9617944%)
// NV:  Trump=663493.56 (49.488243%) Biden=677216.0 (50.51176%) Total=1340709.5 difference=13722.4375 (1.0235176%) [Biden wins NV] uncounted=143645.5 (10.714141%)
// PA:  Trump=3466507.8 (49.95046%) Biden=3473384.0 (50.04954%) Total=6939892.0 difference=6876.25 (0.09908295%) [Biden wins PA] uncounted=354871.0 (5.1134944%)

// Projections for: Fri Nov 06 10:26:05 CST 2020
// AZ:  Trump=1648056.5 (49.345306%) Biden=1691787.9 (50.65469%) Total=3339844.5 difference=43731.375 (1.3093834%) [Biden wins AZ] uncounted=261329.5 (7.8246007%)
// GA:  Trump=2502123.5 (49.8165%) Biden=2520556.2 (50.183495%) Total=5022680.0 difference=18432.75 (0.36699295%) [Biden wins GA] uncounted=123965.0 (2.4681046%)
// NC:  Trump=2870172.0 (50.63125%) Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC] uncounted=281273.0 (4.9617944%)
// NV:  Trump=663493.56 (49.488243%) Biden=677216.0 (50.51176%) Total=1340709.5 difference=13722.4375 (1.0235176%) [Biden wins NV] uncounted=143645.5 (10.714141%)
// PA:  Trump=3469067.2 (49.924465%) Biden=3479564.8 (50.075535%) Total=6948632.0 difference=10497.5 (0.15106964%) [Biden wins PA] uncounted=355307.0 (5.1133375%)

// Projections for: Fri Nov 06 10:40:30 CST 2020
// AZ:  Trump=1638572.8 (49.299374%) Biden=1685146.5 (50.700626%) Total=3323719.2 difference=46573.75 (1.4012527%) [Biden wins AZ] uncounted=245204.25 (7.3774056%)
// GA:  Trump=2502125.5 (49.8165%) Biden=2520558.2 (50.18349%) Total=5022684.0 difference=18432.75 (0.36698914%) [Biden wins GA] uncounted=123965.0 (2.4681027%)
// NC:  Trump=2870172.0 (50.63125%) Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC] uncounted=281273.0 (4.9617944%)
// NV:  Trump=660016.75 (49.143982%) Biden=683009.9 (50.856018%) Total=1343026.6 difference=22993.125 (1.7120361%) [Biden wins NV] uncounted=116610.625 (8.682674%)
// PA:  Trump=3469308.2 (49.925674%) Biden=3479637.5 (50.07432%) Total=6948946.0 difference=10329.25 (0.14864731%) [Biden wins PA] uncounted=355323.0 (5.1133366%)

// Projections for: Fri Nov 06 10:51:58 CST 2020
// AZ:  Trump=1638572.8 (49.299374%) Biden=1685146.5 (50.700626%) Total=3323719.2 difference=46573.75 (1.4012527%) [Biden wins AZ] uncounted=245204.25 (7.3774056%)
// GA:  Trump=2502128.8 (49.816517%) Biden=2520560.2 (50.183483%) Total=5022689.0 difference=18431.5 (0.36696625%) [Biden wins GA] uncounted=123965.0 (2.4681003%)
// NC:  Trump=2870172.0 (50.63125%) Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC] uncounted=281273.0 (4.9617944%)
// NV:  Trump=660076.3 (49.13888%) Biden=683210.94 (50.86112%) Total=1343287.2 difference=23134.625 (1.7222366%) [Biden wins NV] uncounted=116626.25 (8.682153%)
// PA:  Trump=3469308.2 (49.925674%) Biden=3479637.5 (50.07432%) Total=6948946.0 difference=10329.25 (0.14864731%) [Biden wins PA] uncounted=355323.0 (5.1133366%)

// Projections for: Fri Nov 06 11:33:38 CST 2020
// AZ:  Trump=1642446.4 (49.32563%) Biden=1687356.8 (50.674374%) Total=3329803.0 difference=44910.375 (1.3487434%) [Biden wins AZ] uncounted=251288.0 (7.546633%)
// GA:  Trump=2502178.0 (49.81671%) Biden=2520590.5 (50.18329%) Total=5022768.5 difference=18412.5 (0.36657715%) [Biden wins GA] uncounted=123965.5 (2.4680712%)
// NC:  Trump=2870172.0 (50.63125%) Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC] uncounted=281273.0 (4.9617944%)
// NV:  Trump=659219.7 (49.15052%) Biden=682006.56 (50.84948%) Total=1341226.2 difference=22786.875 (1.6989594%) [Biden wins NV] uncounted=109346.25 (8.152707%)
// PA:  Trump=3470206.0 (49.91905%) Biden=3481460.8 (50.080948%) Total=6951667.0 difference=11254.75 (0.16189957%) [Biden wins PA] uncounted=355459.0 (5.1132917%)

// Projections for: Fri Nov 06 13:55:42 CST 2020
// AZ:  Trump=1645065.0 (49.36139%) Biden=1687630.9 (50.638607%) Total=3332696.0 difference=42565.875 (1.2772179%) [Biden wins AZ] uncounted=249250.0 (7.47893%)
// GA:  Trump=2502344.0 (49.81632%) Biden=2520797.2 (50.183685%) Total=5023141.0 difference=18453.25 (0.3673668%) [Biden wins GA] uncounted=123982.0 (2.4682167%)
// NC:  Trump=2870172.0 (50.63125%) Biden=2798604.0 (49.36875%) Total=5668776.0 difference=71568.0 (1.262497%) [Trump wins NC] uncounted=281273.0 (4.9617944%)
// NV:  Trump=678196.3 (49.038773%) Biden=704783.44 (50.961227%) Total=1382979.8 difference=26587.125 (1.9224548%) [Biden wins NV] uncounted=148908.75 (10.767241%)
// PA:  Trump=3472678.5 (49.87818%) Biden=3489641.8 (50.121822%) Total=6962320.0 difference=16963.25 (0.2436409%) [Biden wins PA] uncounted=352636.0 (5.064921%)

// Projections for: Fri Nov 06 15:18:32 CST 2020
// AZ:  Trump=1645968.8 (49.368324%) Biden=1688089.9 (50.63168%) Total=3334058.5 difference=42121.125 (1.2633553%) [Biden wins AZ] uncounted=244061.5 (7.3202524%)
// GA:  Trump=2502555.5 (49.81688%) Biden=2520953.8 (50.183125%) Total=5023509.0 difference=18398.25 (0.36624527%) [Biden wins GA] uncounted=123986.0 (2.4681153%)
// NC:  Trump=2870380.2 (50.630936%) Biden=2798841.8 (49.369064%) Total=5669222.0 difference=71538.5 (1.2618713%) [Trump wins NC] uncounted=281295.0 (4.961792%)
// NV:  Trump=678196.3 (49.038773%) Biden=704783.44 (50.961227%) Total=1382979.8 difference=26587.125 (1.9224548%) [Biden wins NV] uncounted=148908.75 (10.767241%)
// PA:  Trump=3473897.5 (49.863987%) Biden=3492849.0 (50.136013%) Total=6966746.5 difference=18951.5 (0.27202606%) [Biden wins PA] uncounted=351259.5 (5.0419445%)

// Projections for: Fri Nov 06 16:34:00 CST 2020
// AZ:  Trump=1646056.0 (49.372993%) Biden=1687863.8 (50.627007%) Total=3333919.8 difference=41807.75 (1.2540131%) [Biden wins AZ] uncounted=242919.75 (7.2863107%)
// GA:  Trump=2505254.5 (49.78983%) Biden=2526404.5 (50.21017%) Total=5031659.0 difference=21150.0 (0.4203415%) [Biden wins GA] uncounted=124373.0 (2.471809%)
// NC:  Trump=2870380.2 (50.630936%) Biden=2798841.8 (49.369064%) Total=5669222.0 difference=71538.5 (1.2618713%) [Trump wins NC] uncounted=281295.0 (4.961792%)
// NV:  Trump=678196.3 (49.038773%) Biden=704783.44 (50.961227%) Total=1382979.8 difference=26587.125 (1.9224548%) [Biden wins NV] uncounted=148908.75 (10.767241%)
// PA:  Trump=3474727.2 (49.86535%) Biden=3493492.5 (50.134647%) Total=6968220.0 difference=18765.25 (0.26929855%) [Biden wins PA] uncounted=351332.0 (5.0419188%)

// Projections for: Fri Nov 06 18:32:39 CST 2020
// AZ:  Trump=1668901.6 (49.418476%) Biden=1708178.6 (50.581524%) Total=3377080.2 difference=39277.0 (1.1630478%) [Biden wins AZ] uncounted=269223.25 (7.9720716%)
// GA:  Trump=2506213.5 (49.79191%) Biden=2527161.5 (50.20809%) Total=5033375.0 difference=20948.0 (0.41618347%) [Biden wins GA] uncounted=124391.0 (2.471324%)
// NC:  Trump=2870811.8 (50.629128%) Biden=2799465.2 (49.370872%) Total=5670277.0 difference=71346.5 (1.258255%) [Trump wins NC] uncounted=281337.0 (4.9616094%)
// NV:  Trump=676610.94 (48.969803%) Biden=705079.2 (51.030197%) Total=1381690.1 difference=28468.25 (2.0603943%) [Biden wins NV] uncounted=139231.12 (10.076871%)
// PA:  Trump=3475069.8 (49.83765%) Biden=3497710.2 (50.16235%) Total=6972780.0 difference=22640.5 (0.3246994%) [Biden wins PA] uncounted=344484.0 (4.940411%)

// Projections for: Fri Nov 06 20:09:19 CST 2020
// AZ:  Trump=1678324.9 (49.559124%) Biden=1708185.8 (50.44088%) Total=3386510.5 difference=29860.875 (0.8817558%) [Biden wins AZ] uncounted=208237.5 (6.149029%)
// GA:  Trump=2506530.5 (49.791355%) Biden=2527537.0 (50.208645%) Total=5034067.5 difference=21006.5 (0.41728973%) [Biden wins GA] uncounted=124397.5 (2.471113%)
// NC:  Trump=2870901.2 (50.629005%) Biden=2799566.2 (49.370995%) Total=5670467.5 difference=71335.0 (1.2580109%) [Trump wins NC] uncounted=281346.5 (4.9616103%)
// NV:  Trump=676610.94 (48.969803%) Biden=705079.2 (51.030197%) Total=1381690.1 difference=28468.25 (2.0603943%) [Biden wins NV] uncounted=139231.12 (10.076871%)
// PA:  Trump=3476217.0 (49.821754%) Biden=3501090.5 (50.178246%) Total=6977307.5 difference=24873.5 (0.3564911%) [Biden wins PA] uncounted=344710.5 (4.9404516%)

// Biden projected lead in PA grows by 8000 even though number of votes left to count does not increase
// Projections for: Sat Nov 07 09:03:48 CST 2020
// AZ:  Trump=1678324.9 (49.559124%) Biden=1708185.8 (50.44088%) Total=3386510.5 difference=29860.875 (0.8817558%) [Biden wins AZ] uncounted=208237.5 (6.149029%)  currentDiff=29861.0
// GA:  Trump=2507973.8 (49.75806%) Biden=2532362.8 (50.24194%) Total=5040336.5 difference=24389.0 (0.4838791%) [Biden wins GA] uncounted=124674.5 (2.4735353%)  currentDiff=7248.0
// NC:  Trump=2870901.2 (50.629005%) Biden=2799566.2 (49.370995%) Total=5670467.5 difference=71335.0 (1.2580109%) [Trump wins NC] uncounted=281346.5 (4.9616103%)  currentDiff=76515.0
// NV:  Trump=676610.94 (48.969803%) Biden=705079.2 (51.030197%) Total=1381690.1 difference=28468.25 (2.0603943%) [Biden wins NV] uncounted=139231.12 (10.076871%)  currentDiff=22657.0
// PA:  Trump=3478452.2 (49.766106%) Biden=3511148.5 (50.23389%) Total=6989601.0 difference=32696.25 (0.46778488%) [Biden wins PA] uncounted=344660.0 (4.93104%)  currentDiff=28833.0

// PA and NV called for Biden, giving him the presidency
// Projections for: Sat Nov 07 13:01:15 CST 2020
// AZ:  Trump=1707636.4 (49.691643%) Biden=1728829.5 (50.308353%) Total=3436466.0 difference=21193.125 (0.61671066%) [Biden wins AZ] uncounted=203153.0 (5.9116836%)  currentDiff=20573.0
// GA:  Trump=2508045.5 (49.75791%) Biden=2532450.5 (50.24209%) Total=5040496.0 difference=24405.0 (0.48417664%) [Biden wins GA] uncounted=124676.0 (2.4734867%)  currentDiff=7264.0
// NC:  Trump=2870901.2 (50.629005%) Biden=2799566.2 (49.370995%) Total=5670467.5 difference=71335.0 (1.2580109%) [Trump wins NC] uncounted=281346.5 (4.9616103%)  currentDiff=76515.0
// NV:  Trump=673976.2 (48.89714%) Biden=704378.75 (51.102856%) Total=1378355.0 difference=30402.562 (2.2057152%) [Biden wins NV] uncounted=118846.0 (8.622307%)  currentDiff=25699.0
// PA:  Trump=3481058.2 (49.720726%) Biden=3520163.2 (50.279274%) Total=7001221.5 difference=39105.0 (0.558548%) [Biden wins PA] uncounted=344187.5 (4.916106%)  currentDiff=34414.0

// Projections for: Sat Nov 07 15:27:25 CST 2020
// AZ:  Trump=1707821.6 (49.69257%) Biden=1728952.8 (50.307426%) Total=3436774.5 difference=21131.125 (0.6148567%) [Biden wins AZ] uncounted=203168.5 (5.911604%)  currentDiff=20514.0
// GA:  Trump=2508331.5 (49.754997%) Biden=2533034.2 (50.245%) Total=5041366.0 difference=24702.75 (0.49000168%) [Biden wins GA] uncounted=124715.0 (2.4738336%)  currentDiff=7547.0
// NC:  Trump=2871724.2 (50.618656%) Biden=2801528.2 (49.381344%) Total=5673252.5 difference=70196.0 (1.2373123%) [Trump wins NC] uncounted=281485.5 (4.9616246%)  currentDiff=75433.0
// NV:  Trump=673976.2 (48.89714%) Biden=704378.75 (51.102856%) Total=1378355.0 difference=30402.562 (2.2057152%) [Biden wins NV] uncounted=118846.0 (8.622307%)  currentDiff=25699.0
// PA:  Trump=3482682.5 (49.698044%) Biden=3525003.0 (50.301956%) Total=7007685.5 difference=42320.5 (0.60391235%) [Biden wins PA] uncounted=344510.5 (4.916181%)  currentDiff=37469.0

// Projections for: Sat Nov 07 18:10:38 CST 2020
// AZ:  Trump=1707629.4 (49.68684%) Biden=1729154.6 (50.31316%) Total=3436784.0 difference=21525.25 (0.6263199%) [Biden wins AZ] uncounted=202168.0 (5.8824763%)  currentDiff=21188.0
// GA:  Trump=2508510.2 (49.738945%) Biden=2534842.2 (50.261055%) Total=5043352.5 difference=26332.0 (0.52211%) [Biden wins GA] uncounted=124734.5 (2.4732456%)  currentDiff=9160.0
// NC:  Trump=2871809.5 (50.618393%) Biden=2801641.0 (49.381607%) Total=5673450.5 difference=70168.5 (1.2367859%) [Trump wins NC] uncounted=281495.5 (4.961628%)  currentDiff=75407.0
// NV:  Trump=673976.2 (48.89714%) Biden=704378.75 (51.102856%) Total=1378355.0 difference=30402.562 (2.2057152%) [Biden wins NV] uncounted=118846.0 (8.622307%)  currentDiff=25699.0
// PA:  Trump=3483085.5 (49.69935%) Biden=3525226.2 (50.300648%) Total=7008312.0 difference=42140.75 (0.6012993%) [Biden wins PA] uncounted=344542.0 (4.916191%)  currentDiff=37298.0

// Projections for: Sun Nov 08 07:42:57 CST 2020
// AZ:  Trump=1708737.1 (49.723793%) Biden=1727720.6 (50.276207%) Total=3436457.8 difference=18983.5 (0.55241394%) [Biden wins AZ] uncounted=192677.75 (5.606871%)  currentDiff=18610.0
// GA:  Trump=2509116.5 (49.728256%) Biden=2536539.0 (50.271744%) Total=5045655.5 difference=27422.5 (0.54348755%) [Biden wins GA] uncounted=124849.5 (2.474396%)  currentDiff=10196.0
// NC:  Trump=2871809.5 (50.618393%) Biden=2801641.0 (49.381607%) Total=5673450.5 difference=70168.5 (1.2367859%) [Trump wins NC] uncounted=281495.5 (4.961628%)  currentDiff=75407.0
// NV:  Trump=672614.44 (48.858536%) Biden=704042.56 (51.141464%) Total=1376657.0 difference=31428.125 (2.2829285%) [Biden wins NV] uncounted=109239.0 (7.935092%)  currentDiff=27530.0
// PA:  Trump=3483085.5 (49.69935%) Biden=3525226.2 (50.300648%) Total=7008312.0 difference=42140.75 (0.6012993%) [Biden wins PA] uncounted=344542.0 (4.916191%)  currentDiff=37298.0

// Projections for: Mon Nov 09 05:58:55 CST 2020
// AZ:  Trump=1714834.8 (49.752766%) Biden=1731877.9 (50.24724%) Total=3446712.5 difference=17043.125 (0.4944725%) [Biden wins AZ] uncounted=176369.5 (5.1170354%)  currentDiff=16985.0
// GA:  Trump=2507618.2 (49.723442%) Biden=2535512.8 (50.276558%) Total=5043131.0 difference=27894.5 (0.55311584%) [Biden wins GA] uncounted=121922.0 (2.4175854%)  currentDiff=10353.0
// NC:  Trump=2871809.5 (50.618393%) Biden=2801641.0 (49.381607%) Total=5673450.5 difference=70168.5 (1.2367859%) [Trump wins NC] uncounted=281495.5 (4.961628%)  currentDiff=75407.0
// NV:  Trump=674115.94 (48.67414%) Biden=710841.1 (51.325863%) Total=1384957.0 difference=36725.188 (2.651722%) [Biden wins NV] uncounted=90916.0 (6.564536%)  currentDiff=34283.0
// PA:  Trump=3485706.8 (49.65564%) Biden=3534053.5 (50.344364%) Total=7019760.0 difference=48346.75 (0.6887245%) [Biden wins PA] uncounted=345114.0 (4.9163218%)  currentDiff=43194.0

//Arizona projected to win by 13,000 votes, although current return suggest it would be much closer
// Projections for: Tue Nov 10 21:48:08 CST 2020
// AZ:  Trump=1728601.9 (49.803932%) Biden=1742212.1 (50.196068%) Total=3470814.0 difference=13610.25 (0.39213562%) [Biden wins AZ] uncounted=173243.0 (4.9914227%)  currentDiff=12813.0
// GA:  Trump=2470468.2 (49.82014%) Biden=2488305.5 (50.17985%) Total=4958774.0 difference=17837.25 (0.3597107%) [Biden wins GA] uncounted=29159.0 (0.58802843%)  currentDiff=14149.0
// NC:  Trump=2871382.8 (50.619427%) Biden=2801108.8 (49.380573%) Total=5672491.5 difference=70274.0 (1.2388535%) [Trump wins NC] uncounted=278097.5 (4.9025636%)  currentDiff=74870.0
// NV:  Trump=670169.25 (48.64634%) Biden=707466.3 (51.353664%) Total=1377635.5 difference=37297.062 (2.707325%) [Biden wins NV] uncounted=71507.5 (5.1905966%)  currentDiff=36726.0
// PA:  Trump=3497201.2 (49.638588%) Biden=3548126.8 (50.361412%) Total=7045328.0 difference=50925.5 (0.7228241%) [Biden wins PA] uncounted=346338.0 (4.9158535%)  currentDiff=45616.0

// pa Projects 6000 a difference greater than projected by current returns
// Projections for: Wed Nov 11 08:59:35 CST 2020
// AZ:  Trump=1728601.9 (49.803932%) Biden=1742212.1 (50.196068%) Total=3470814.0 difference=13610.25 (0.39213562%) [Biden wins AZ] uncounted=173243.0 (4.9914227%)  currentDiff=12813.0
// GA:  Trump=2469674.2 (49.820385%) Biden=2487481.5 (50.179607%) Total=4957156.0 difference=17807.25 (0.3592224%) [Biden wins GA] uncounted=27457.0 (0.5538861%)  currentDiff=14111.0
// NC:  Trump=2871382.8 (50.619427%) Biden=2801108.8 (49.380573%) Total=5672491.5 difference=70274.0 (1.2388535%) [Trump wins NC] uncounted=278097.5 (4.9025636%)  currentDiff=74870.0
// NV:  Trump=670169.25 (48.64634%) Biden=707466.3 (51.353664%) Total=1377635.5 difference=37297.062 (2.707325%) [Biden wins NV] uncounted=71507.5 (5.1905966%)  currentDiff=36726.0
// PA:  Trump=3498113.8 (49.624043%) Biden=3551118.2 (50.375957%) Total=7049232.0 difference=53004.5 (0.751915%) [Biden wins PA] uncounted=346533.0 (4.9158974%)  currentDiff=47591.0

// Projections for: Thu Nov 12 16:05:57 CST 2020
// AZ:  Trump=1728099.0 (49.749084%) Biden=1745530.8 (50.250916%) Total=3473629.8 difference=17431.75 (0.50183105%) [Biden wins AZ] uncounted=152775.75 (4.3981586%)  currentDiff=11390.0
// GA:  Trump=2467192.5 (49.82252%) Biden=2484770.0 (50.17748%) Total=4951962.5 difference=17577.5 (0.35495758%) [Biden wins GA] uncounted=22055.5 (0.44538906%)  currentDiff=14057.0
// NC:  Trump=2874385.5 (50.60349%) Biden=2805826.5 (49.39651%) Total=5680212.0 difference=68559.0 (1.2069778%) [Trump wins NC] uncounted=278480.0 (4.902634%)  currentDiff=73244.0
// NV:  Trump=670451.8 (48.638546%) Biden=707985.5 (51.36146%) Total=1378437.2 difference=37533.688 (2.7229156%) [Biden wins NV] uncounted=71393.25 (5.179289%)  currentDiff=36866.0
// PA:  Trump=3509894.5 (49.573452%) Biden=3570295.2 (50.426544%) Total=7080190.0 difference=60400.75 (0.8530922%) [Biden wins PA] uncounted=348073.0 (4.9161534%)  currentDiff=54613.0

// Projections for: Thu Nov 12 22:06:37 CST 2020
// AZ:  Trump=1728214.0 (49.759018%) Biden=1744953.6 (50.240986%) Total=3473167.5 difference=16739.625 (0.48196793%) [Biden wins AZ] uncounted=151211.5 (4.353706%)  currentDiff=11034.0
// GA:  Trump=2465826.2 (49.842514%) Biden=2481409.0 (50.157494%) Total=4947235.0 difference=15582.75 (0.31497955%) [Biden wins GA] uncounted=17088.0 (0.34540507%)  currentDiff=14149.0
// NC:  Trump=2878269.2 (50.593117%) Biden=2810784.0 (49.406887%) Total=5689053.0 difference=67485.25 (1.1862297%) [Trump wins NC] uncounted=271854.0 (4.778546%)  currentDiff=71399.0
// NV:  Trump=670451.8 (48.638546%) Biden=707985.5 (51.36146%) Total=1378437.2 difference=37533.688 (2.7229156%) [Biden wins NV] uncounted=71393.25 (5.179289%)  currentDiff=36866.0
// PA:  Trump=3512663.0 (49.572956%) Biden=3573182.5 (50.427044%) Total=7085845.5 difference=60519.5 (0.8540878%) [Biden wins PA] uncounted=348355.5 (4.9162164%)  currentDiff=54726.0

// Projections for: Fri Nov 13 18:20:12 CST 2020
// AZ:  Trump=1732422.2 (49.759777%) Biden=1749149.4 (50.240227%) Total=3481571.5 difference=16727.125 (0.48044968%) [Biden wins AZ] uncounted=151631.5 (4.3552604%)  currentDiff=11022.0
// GA:  Trump=2462779.8 (49.8727%) Biden=2475352.0 (50.127296%) Total=4938132.0 difference=12572.25 (0.2545967%) [Biden wins GA] uncounted=7940.0 (0.16078955%)  currentDiff=14172.0
// NC:  Trump=2760890.5 (50.673973%) Biden=2687450.0 (49.326027%) Total=5448340.5 difference=73440.5 (1.3479462%) [Trump wins NC] uncounted=12394.5 (0.22749129%)  currentDiff=73698.0
// NV:  Trump=674635.9 (48.695316%) Biden=710786.56 (51.30468%) Total=1385422.5 difference=36150.688 (2.6093636%) [Biden wins NV] uncounted=69643.5 (5.026878%)  currentDiff=34529.0
// PA:  Trump=3521524.5 (49.513268%) Biden=3590760.0 (50.486732%) Total=7112284.5 difference=69235.5 (0.97346497%) [Biden wins PA] uncounted=349677.5 (4.9165287%)  currentDiff=63005.0

// Projections for: Tue Jan 05 12:03:26 CST 2021
// AZ:  Trump=1661686.0 (49.84317%) Biden=1672143.0 (50.15683%) Total=3333829.0 difference=10457.0 (0.31365967%) [Biden wins AZ] uncounted=0.0 (0.0%)  currentDiff=10457.0
// GA:  Trump=2462003.2 (49.881325%) Biden=2473718.2 (50.118675%) Total=4935721.5 difference=11715.0 (0.23735046%) [Biden wins GA] uncounted=234.5 (0.0047510783%)  currentDiff=11779.0
// NC:  Trump=2758966.0 (50.683685%) Biden=2684533.0 (49.316315%) Total=5443499.0 difference=74433.0 (1.3673706%) [Trump wins NC] uncounted=434.0 (0.007972813%)  currentDiff=74481.0
// NV:  Trump=673170.1 (48.85579%) Biden=704701.7 (51.144215%) Total=1377871.8 difference=31531.562 (2.2884254%) [Biden wins NV] uncounted=4495.75 (0.32628217%)  currentDiff=33596.0
// PA:  Trump=3383689.0 (49.412563%) Biden=3464142.5 (50.587437%) Total=6847831.5 difference=80453.5 (1.1748734%) [Biden wins PA] uncounted=9645.5 (0.1408548%)  currentDiff=81660.0
