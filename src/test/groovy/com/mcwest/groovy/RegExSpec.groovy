package com.mcwest.groovy

import spock.lang.Specification

class RegExSpec extends Specification{

    def 'test'() {
        given:
        def pattern = '#option1\\s*=\\s*[A-Za-z1-9]*'

        expect:
        // no need to escape chars if put directly into pattern
        ('.. #option1= Dv1' =~ /#option1\s*=\s*[A-Za-z1-9]*/).count == 1

        // must escape characters if declared as string
        ('xs.#option1 = Dv1' =~ /${pattern}/)[0] == '#option1 = Dv1'
    }
}
