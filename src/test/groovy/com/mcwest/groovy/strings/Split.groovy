package com.mcwest.groovy.collections

import spock.lang.Specification
import spock.lang.Unroll

class Split extends Specification {

    @Unroll
    def 'split can parse a url into it\'s segments'() {
        when:
            String url = "http://localhost:3000/portal-web#/project/1735/results/"
            String urlMinusProtocol = url.substring(url.indexOf('//') + 2)
            println 'urlMinus = ' + urlMinusProtocol
            String[] split = urlMinusProtocol.split(/\//)

        then:
            split[index] == expected

        where:
            index | expected
            0     | 'localhost:3000'
            1     | 'portal-web#'
            2     | 'project'
            3     | '1735'
            4     | 'results'
    }
}
