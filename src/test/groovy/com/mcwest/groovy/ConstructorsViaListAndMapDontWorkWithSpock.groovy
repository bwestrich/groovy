package com.mcwest.groovy

import org.codehaus.groovy.runtime.typehandling.GroovyCastException
import spock.lang.Specification

class TestSpec extends Specification {

    def '''
Spock does not support constructors via list or map.
Too bad!
'''() {

        expect:
        Kevin k = new Kevin('Kevin')
        k

        try {
            Kevin k1 = ['Kevin']
            false // ('should not get here')
        } catch (GroovyCastException e) {
        }

    }


    class Kevin {

        String name
        Kevin(String name) {
            this.name = name
        }
    }

}