import spock.lang.Specification
import spock.lang.Unroll

class DeepCopySpec extends Specification {

    def "groovy map deep copy does not work on nested maps "() {
        setup:
        Map s1 = [keyTop: [key2: "foo"]]
        Map s1clone = s1.clone()

        expect:
        s1 == s1clone
        !s1.is(s1clone)

        s1.keyTop.key2.is(s1clone.keyTop.key2) == true  // should equal false
    }

    def deepcopy(orig) {
        //ref: http://stackoverflow.com/questions/13155127/deep-copy-map-in-groovy
        def bos = new ByteArrayOutputStream()
        def oos = new ObjectOutputStream(bos)
        oos.writeObject(orig); oos.flush()
        def bin = new ByteArrayInputStream(bos.toByteArray())
        def ois = new ObjectInputStream(bin)
        return ois.readObject()
    }

    def "custom deep copy does work on nested maps "() {
        setup:
        Map s1 = [keyTop: [key2: "foo"]]
        Map s1clone = deepcopy(s1)

        expect:
        s1 == s1clone
        !s1.is(s1clone)
        !s1.keyTop.key2.is(s1clone.keyTop.key2)   // passes
    }
}
