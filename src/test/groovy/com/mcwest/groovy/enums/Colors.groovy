package com.mcwest.groovy.enums


enum Colors {

    BLUE('Blue'),
    RED('red')

    String id

    Colors(String id) {
        this.id = id
    }

    static Colors byId(String id) {
        values().find { it.id == id }
    }

}