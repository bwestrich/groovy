package com.mcwest.groovy.enums

import spock.lang.Specification

class ColorsSpec extends Specification{

    def 'enum to id' () {
        expect:
        Colors.BLUE.id == 'Blue'
    }

    def 'enum to name' () {
        expect:
        Colors.BLUE.toString() == 'BLUE'
    }

    def 'id to enum' () {
        expect:
        Colors.byId('Blue') == Colors.BLUE
    }

    def 'name to enum' () {
        expect:
        'BLUE' as Colors == Colors.BLUE
    }

}
