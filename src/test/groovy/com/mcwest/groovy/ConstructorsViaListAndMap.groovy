package com.mcwest.groovy

/**
 * demo constructing a class using a list or map.
 * 1. can initialize any class using map, and class with constructor using list.
 * 2. don't need to specify class type on rhs of equation.
 * based on discussions with Kevin Beatty.
 */
class ConstructorsViaListAndMap { // feature doesn't work with Spock

    static void main(args) {
        create()
    }

    static create() {

        SampleClassWithConstructor k

        // use constructor args
        k = new SampleClassWithConstructor('SampleClass')
        println k.name

        // use list
        SampleClassWithConstructor k1 = ['Kevin1']
        println k1.name

        // use def on lhs, not as useful as prior example
        def k2 = ['Kevin2'] as SampleClassWithConstructor
        println k2.name

        // can reassign class with new constructor invocation
        k = ['Kevin3']
        println k.name

        //can use map
        k = [name: 'KevinWithMap']
        println k.name

        // can initialize values later
        SampleClass k3 = []
        k3.name = 'KevinAfterNullIsGivenValue'
        println k3.name

        // can use map when no constructor
        SampleClass kAddress = [name: "SampleClass", address: "Lakeville"]
        println kAddress.address

    }

    static class SampleClass {
        String name
        String address
    }

    static class SampleClassWithConstructor {
        String name
        String address

        SampleClassWithConstructor(
                String name
        ) {
            this.name = name
        }
    }


}