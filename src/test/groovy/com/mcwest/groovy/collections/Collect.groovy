package com.mcwest.groovy.collections

import spock.lang.Specification

class Collect extends Specification {

    def 'collect creates a new collection whose elements equal the closure\'s expression'() {
        given:
            Collection idMaps = [[id: 1], [id: 3]]

        when:
            def ids = idMaps.collect {
                it.id
            }

        then:
            ids == [1, 3]

        and: 'initial collection is unchanged'
            idMaps[0].id == 1

    }

    def 'can create a list from keys in a map'() {
        given:
            Map map = ['test': 1, 'test2': 2]

        when:
            List keys = map.collect {
                it.key // can also use it.value
            }

        then:
            keys == ['test', 'test2']

    }

    def 'collect is a handy way to initialize a fixed length array'() {
        given:
            def list = new Integer[2]

        when:
            list = list.collect { 5 }

        then:
            list == [5, 5]
    }

    def 'collect does not create a map, use collectEntries for that'() {
        given:
            Collection ids = [1, 3]

        when:
            def attemptedMap = ids.collect { [(it): it] }

        then:
            attemptedMap != [1: 1, 3: 3]

    }

}
