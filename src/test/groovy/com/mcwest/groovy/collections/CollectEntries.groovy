package com.mcwest.groovy.collections

import spock.lang.Specification

class CollectEntries extends Specification {

    def 'collectEntries creates a map from a list'() {
        given:
            Collection ids = [1, 4]

        when:
            def isEven = ids.collectEntries { [(it): (it % 2) == 0] }

        then:
            isEven == [1: false, 4: true]
    }

}
