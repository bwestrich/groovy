package com.mcwest.groovy.collections

import spock.lang.Specification
import spock.lang.Unroll

class Every extends Specification {

    @Unroll
    def 'every finds whether all characters pass a certain rule: #word'() {
        when:
            def hasDash = word.every { it == '-' }

        then:
            hasDash == containsAllDashes

        where:
            word | containsAllDashes
            '-'  | true
            '--' | true
            '-1' | false
    }
}
