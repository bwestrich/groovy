package com.mcwest.groovy.collections

import spock.lang.Specification

class ListOfProperties extends Specification {

    def 'can use dot notation to create a list of list properties'() {
        when:
            def persons = [new Person(first: 'Joe', last: 'Mauer'), new Person(first: 'Karl', last: 'Walz')]

        then:
            persons.first == ['Joe', 'Karl']
    }

    class Person {
        String first, last;
    }
}
