package com.mcwest.groovy.collections

import spock.lang.Specification

class Maps extends Specification {

    def 'can append a map to another map'() {
        expect:
            [one: 1] << [two: 2] == [one: 1, two: 2]
            [one: 1] + [two: 2] == [one: 1, two: 2]
    }
}
