package com.mcwest.groovy.collections

import spock.lang.Specification

class Math extends Specification {

    def 'summing nulls'() {
        when:
            [1, null].sum()

        then:
            Exception e = thrown(GroovyRuntimeException)
            e.message.contains('Ambiguous method overloading for method java.lang.Integer#plus')
    }
}
