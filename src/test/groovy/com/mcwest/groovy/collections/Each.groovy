package com.mcwest.groovy.collections

import spock.lang.Specification

class Each extends Specification {

    def 'each modifies a collection in place'() {
        given:
            Collection idMaps = [[id: 1], [id: 3]]

        when:
            idMaps.each { it.id++ }

        then:
            idMaps == [[id: 2], [id: 4]]
    }

}
