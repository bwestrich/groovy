package com.mcwest.groovy.collections

import spock.lang.Specification


class Find extends Specification {

    // ref: http://mrhaki.blogspot.com/2009/10/groovy-goodness-finding-data-in.html
    def 'find finds the first matching element in a collection'() {
        given:
            Collection idMaps = [[id: 1], [id: 3], [id: 5], [id: 7]]

        when:
            def firstMapWithIdGreaterThan3 = idMaps.find { it.id > 3 }

        then:
            firstMapWithIdGreaterThan3 == [id: 5]
    }

    def 'find returns null if no items match'() {
        given:
            Collection idMaps = [[id: 1], [id: 3], [id: 5], [id: 7]]

        when:
            def firstMapWithIdGreaterThan8 = idMaps.find { it.id > 8 }

        then:
            firstMapWithIdGreaterThan8 == null
    }

}
