package com.mcwest.groovy.collections

import spock.lang.Specification
import spock.lang.Unroll

class Any extends Specification {

    @Unroll
    def 'any can find whether a word has any characters other than a particular one: #word'() {
        when:
            def hasDash = word.any { it != '-' }

        then:
            hasDash == hasOtherCharacters

        where:
            word | hasOtherCharacters
            '-'  | false
            '--' | false
            '-1' | true
    }
}
