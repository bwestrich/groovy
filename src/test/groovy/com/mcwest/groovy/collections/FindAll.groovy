package com.mcwest.groovy.collections

import spock.lang.Specification


class FindAll extends Specification {

    def 'findAll filters a collection'() {
        given:
            Collection idMaps = [[id: 1], [id: 3], [id: 5], [id: 7]]

        when:
            def idMapsGreaterThan3 = idMaps.findAll { it.id > 3 }

        then:
            idMapsGreaterThan3 == [[id: 5], [id: 7]]
    }

}
