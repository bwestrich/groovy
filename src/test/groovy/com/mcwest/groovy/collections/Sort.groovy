package com.mcwest.groovy.collections

import spock.lang.Specification


class Sort extends Specification {

    def 'simple sort'() {
        given:
            Collection idMaps = [[id: 1], [id: 5], [id: 3]]

        when:
            def sorted = idMaps.sort { it.id }

        then:
            sorted == [[id: 1], [id: 3], [id: 5]]
    }

    def 'sort by 2 props using order by'() {
        given:
            Collection idMaps = [[id: 1, subId: 2], [id: 1, subId: 1], [id: 2, subId: 1]]
            def orderByIdSub = new OrderBy([{ it.id }, { it.subId }])

        when: 'call the sort method'
            idMaps.sort(orderByIdSub)

        then: 'we sort in place'
            idMaps == [[id: 1, subId: 1], [id: 1, subId: 2], [id: 2, subId: 1]]
    }

    def 'sort by 2 props using spaceship/elvis operators'() {
        given:
            Collection idMaps = [[id: 1, subId: 2], [id: 1, subId: 1], [id: 2, subId: 1]]

        when: 'call the sort method'
            idMaps.sort { a, b -> a.id <=> b.id ?: a.subId <=> b.subId
            }

        then: 'we sort in place'
            idMaps == [[id: 1, subId: 1], [id: 1, subId: 2], [id: 2, subId: 1]]
    }

}
