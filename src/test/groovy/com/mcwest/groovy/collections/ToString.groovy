package com.mcwest.groovy.collections

import spock.lang.Specification

class ToString extends Specification {

    def 'collections toString shows each item'() {
        when:
            def collection = ['a', 'list', 'of', 'words']

        then:
            collection.toString() == '[a, list, of, words]'

    }
}
