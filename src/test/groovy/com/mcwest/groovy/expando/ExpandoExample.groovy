package com.mcwest.groovy.expando
/*

Expando e = new Expando()

e.name = 'fasdfadsfk'
e.speak = { -> "$name says hello"}

e.speak()

*/

// each class has a metaClass, which is an expando 
class Cat {}
Cat.metaClass.name = 'Kitty'
Cat.metaClass.speak = {String msg = 'meow' -> "$name says $msg" }
new Cat().speak()
