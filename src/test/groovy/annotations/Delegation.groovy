/*
Delegation much more resilient to changes than is inheritance.
 */

class Worker {
    def work(){ println 'working...'}
    def vacation() {println 'coding'}
}

class Analyst {
    def work(){ println 'working 2...'}
    def analyze(){ println 'analyzing...'}
}

class Manager {

    // order matters, the 2nd delegate won't override methods in the first delegate
    @Delegate Worker victim = new Worker()
    @Delegate Analyst victim2 = new Analyst()
    
    def vacation() {println 'not working, all year long...'}
}

//def bob = new Worker()
//def bob = new Analyst()
def bob = new Manager()
bob.work()
bob.vacation()