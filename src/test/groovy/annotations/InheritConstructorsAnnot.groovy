import groovy.transform.InheritConstructors

class Base {
  Base(int  value) {}
  Base(String value) {}
}

@InheritConstructors
class Derived extends Base {
//  Derived(int  value) { super(value) }
//  Derived(String value) { super(value) }
}

def d1 = new Derived(1)

