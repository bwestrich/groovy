import groovy.transform.TailRecursive

// SICP (Structure Interpretation of Computer Programs)
// procedure: source code
// process: what runs (compiled code)

// this annotation turns a recursive procedure into an iterative process


//def factorial(n) {
//    if (n == 1)
//        1
//    else
//        n * factorial(n - 1)
//}
//
//println factorial(5)

@TailRecursive
def factorial(BigInteger fact, n) {
    if(n == 1)
      fact
    else
      factorial(n * fact, n - 1)
}

println factorial(1, 5000)

