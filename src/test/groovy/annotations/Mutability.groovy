package annotations

import groovy.transform.*

@Immutable
class ImmutableCreditCard {
    String number
    int amount
}

//def cc = new annotations.mutability.annotations.newify.ImmutableCreditCard('number', 3)
def cc = new ImmutableCreditCard(number: 'number')
//cc.amount = 2000
println cc