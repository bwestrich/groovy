import groovy.transform.*

// purity of functions: 
//  will not change stuff, only return value
//  will not depend on anything that is changing


@Memoized // caching
def fib(n) {
  if (n < 2) 
      1
  else 
      fib(n - 1) + fib(n - 2)      
}

println(fib(100))