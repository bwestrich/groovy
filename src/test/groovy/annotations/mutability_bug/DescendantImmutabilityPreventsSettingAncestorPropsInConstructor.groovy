package annotations.mutability_bug

import spock.lang.Specification

class DescendantImmutabilityPreventsSettingAncestorPropsInConstructor extends Specification {

    def 'test'() {
        when:
        Descendant2 d = new Descendant2(ancestorProperty: "a", descendantProperty: "d")

        then:
        d.descendantProperty

        and: 'ancestor properties are ignored in descendant constructor'
        // noted on groovy-lang.org 9/19/2013:
        // http://www.groovy-lang.org/mailing-lists.html#nabble-td5716924
        !d.ancestorProperty
    }
}
