package annotations.mutability_bug

import groovy.transform.Immutable

class Ancestor2 {
    String ancestorProperty
}

@Immutable
class Descendant2 extends Ancestor2 {
    String descendantProperty
}


def d = new Descendant2(ancestorProperty: "a")

println d.ancestorProperty