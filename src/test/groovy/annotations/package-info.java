/**
 * Examples from Venkat's SpringOne2GX session.
 *
 * These annotations might be useful in some projects, but aren't useful for fp due to various limitations
 * described in a few 9/17/2015 checkins to this project.
 */
package annotations;

