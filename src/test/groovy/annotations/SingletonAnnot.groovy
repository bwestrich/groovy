@Singleton(strict = false, lazy = true)
class Util { 
    private Util() {println 'created...'}
    String hello = 'hello'
}

println 'creating...'

//def inst1 = new Util()
//def inst2 = new Util()
def inst1 = Util.instance
def inst2 = Util.instance

println inst1.hello
println inst2.hello

println inst1.is(inst2)    
    