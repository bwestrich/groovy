import groovy.transform.* 
import groovy.transform.builder.* 

//@ToString
@Canonical
//@Canonical (excludes = 'age')
@Builder
class MyPerson {
  String name
  int age

/* default canonical toStrings are fairly worthless, as it only shows you the args passed into the constructor
    so you don't know which property belongs to which value.
     */

public static void main(String[] args) {
  //MyPerson bob = new MyPerson(name: 'Bob')
  //MyPerson bob = MyPerson.builder().name('Robert').age(2).build()
  MyPerson bob = MyPerson.builder().with { 
   name = 'Bob'
   age = 2
   build()
  }
  
  println 'here is bob ' + bob  // prints   'here is bob MyPerson(Bob, 2)'
}

}


 