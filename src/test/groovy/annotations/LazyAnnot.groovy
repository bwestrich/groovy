class Heavy {
    Heavy() {println 'creating heavy...' }
    int size
}

class User {
//   Heavy heavy = new Heavy()
    @Lazy Heavy heavy = new Heavy()
}

println 'creating...'
def user = new User()
println 'calling...'
println user.heavy.size