package annotations

import groovy.transform.*

@Canonical
class NewifiableCreditCard {
    String number
    int amount
}

// ruby
@Newify
def factory1() {
    NewifiableCreditCard.new('111', 23)
}

// python, scala
@Newify(NewifiableCreditCard)
def factory2() {
    NewifiableCreditCard('111', 23)
}

println factory1()
println factory2()